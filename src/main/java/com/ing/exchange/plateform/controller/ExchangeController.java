package com.ing.exchange.plateform.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


import com.ing.exchange.plateform.entity.TransactionEntity;
import com.ing.exchange.plateform.helper.Mapper;
import com.ing.exchange.plateform.model.TransactionHistory;
import com.ing.exchange.plateform.model.TransactionRequest;
import com.ing.exchange.plateform.model.TransactionResponse;
import com.ing.exchange.plateform.service.ExchangeService;


@RestController
@RequestMapping("/transaction")
public class ExchangeController {
	
	@Autowired
	private ExchangeService service;
	
	@Autowired
	private Mapper mapper;
	
	/**
	 * This method will be used to retrieve transaction history.
	 * @param userId
	 * @param type
	 * @return
	 */
	@RequestMapping(value= "/history/user/{userId}/transaction/{tType}")
	public ResponseEntity<TransactionResponse> retrieveData(@PathVariable("userId") String userId, @PathVariable("tType") String type) {
		List<TransactionEntity> transactionEntity = service.retrieveTransaction(userId, type);
		List<TransactionHistory> transactionHistory = mapper.convertTransactionEntityToHistoryList(transactionEntity, userId);
		return new ResponseEntity<>(mapper.createTransactionResponse(transactionHistory),HttpStatus.ACCEPTED);
	}
	
	/**
	 * This Method will be used to make payments.
	 * @param request
	 * @return
	 */
	@RequestMapping(value= "/payments")
	public ResponseEntity<TransactionResponse> makePayments(@RequestBody TransactionRequest request) {
		Integer value = service.submitTransaction(request);
		return new ResponseEntity<>(mapper.createTransactionResponse(value),HttpStatus.ACCEPTED);
	}
	
	/**
	 * This Method will be used to make payments.
	 * @param request
	 * @return
	 */
	@RequestMapping(value= "/payments/received", method = RequestMethod.POST)
	public ResponseEntity<TransactionResponse> receievePayments(@RequestBody TransactionRequest request) {
		Integer value = service.receieveTransaction(request);
		return new ResponseEntity<>(mapper.createTransactionResponse(value),HttpStatus.ACCEPTED);
	}
}
