package com.ing.exchange.plateform.exception;

import java.util.List;

import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.ResponseEntity.BodyBuilder;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;


@ControllerAdvice
public class ExceptionHandling {

	/**
	 * Added for validation errors.
	 * @param ex
	 * @return
	 */
	@ExceptionHandler(ValidationErrors.class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ResponseBody
	public List<Error> processValidationErrors(ValidationErrors ex) {
		
		return ex.getErrorList();
	}
	
	@ExceptionHandler(Exception.class)
	public ResponseEntity<ErrorVM> processRuntimeException(Exception ex) throws Exception {
		BodyBuilder builder;
		ErrorVM errorVM;
		ResponseStatus responseStatus = AnnotationUtils.findAnnotation(ex.getClass(), ResponseStatus.class);
		if (responseStatus != null) {
			builder = ResponseEntity.status(responseStatus.value());
			errorVM = new ErrorVM("error." + responseStatus.value().value(), responseStatus.reason());
		} else {
			builder = ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR);
			errorVM = new ErrorVM("error.internalServerError", "Internal server error");
		}
		return builder.body(errorVM);
	}
	
}
