package com.ing.exchange.plateform.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="Transaction")
public class TransactionEntity {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="transaction_id")
	private Integer transactionId;
	
	@Column(name="user_id")
	private String sendingUser;


	@Column(name="other_user_id")
	private String receivingUser;
	

	@Column(name="amount")
	private Integer amountSent;
	

	@Column(name="transaction_description")
	private String description;
	

	@Column(name="transaction_time")
	private String timePlaced;
	
	

	/**
	 * @return the transactionId
	 */
	public Integer getTransactionId() {
		return transactionId;
	}


	/**
	 * @param transactionId the transactionId to set
	 */
	public void setTransactionId(Integer transactionId) {
		this.transactionId = transactionId;
	}


	/**
	 * @return the sendingUser
	 */
	public String getSendingUser() {
		return sendingUser;
	}


	/**
	 * @param sendingUser the sendingUser to set
	 */
	public void setSendingUser(String sendingUser) {
		this.sendingUser = sendingUser;
	}


	/**
	 * @return the receivingUser
	 */
	public String getReceivingUser() {
		return receivingUser;
	}


	/**
	 * @param receivingUser the receivingUser to set
	 */
	public void setReceivingUser(String receivingUser) {
		this.receivingUser = receivingUser;
	}


	/**
	 * @return the amountSent
	 */
	public Integer getAmountSent() {
		return amountSent;
	}


	/**
	 * @param amountSent the amountSent to set
	 */
	public void setAmountSent(Integer amountSent) {
		this.amountSent = amountSent;
	}


	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}


	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}


	/**
	 * @return the timePlaced
	 */
	public String getTimePlaced() {
		return timePlaced;
	}


	/**
	 * @param timePlaced the timePlaced to set
	 */
	public void setTimePlaced(String timePlaced) {
		this.timePlaced = timePlaced;
	}
	
	
}
