package com.ing.exchange.plateform.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;
import com.ing.exchange.plateform.entity.TransactionEntity;
import com.ing.exchange.plateform.entity.UserEntity;
import com.ing.exchange.plateform.exception.Error;
import com.ing.exchange.plateform.exception.ValidationErrors;
import com.ing.exchange.plateform.helper.Mapper;
import com.ing.exchange.plateform.helper.TransactionType;
import com.ing.exchange.plateform.model.TransactionRequest;
import com.ing.exchange.plateform.repository.TransactionRepositoy;
import com.ing.exchange.plateform.repository.UserRepository;
import com.ing.exchange.plateform.service.ExchangeService;

@Service
public class ExchangeServiceImpl implements ExchangeService {

	public RestTemplate template = new RestTemplate();
	
	@Inject
	private TransactionRepositoy transactionRepositoy;
	
	@Inject
	private UserRepository userRepository;
	
	@Autowired
	private Mapper mapper;
	
	
	
	/**
	 * This method will retrieve transaction data from table.
	 */
	@Override
	public List<TransactionEntity> retrieveTransaction(String userId, String type) {
		List<TransactionEntity> transactionList;
		if(TransactionType.C.toString().equalsIgnoreCase(type)) {
			transactionList = transactionRepositoy.findBasedOnTransactionTypeCredited(userId);
		} else if(TransactionType.D.toString().equalsIgnoreCase(type)) {
			transactionList = transactionRepositoy.findBasedOnTransactionTypeDebited(userId);
		} else {
			transactionList = transactionRepositoy.findForAllTransactions(userId);
		}
		return transactionList;
	}

	/**
	 * This method will submit transaction.
	 */
	@Override
	public Integer submitTransaction(TransactionRequest request) {
		UserEntity senderEntity = null;
		try {
			 senderEntity = userRepository.findByUserId(request.getSendingId());
		} catch(Exception ex) {
			ex.printStackTrace();
		}
		UserEntity receiverEntity = userRepository.findByUserId(request.getReceivingId());
		if(senderEntity != null && receiverEntity != null) {
			if(senderEntity.getBalance() < request.getTotalAmount()) {
				TransactionEntity tEntity = mapper.convertRequestToTransactionEntity(request);
				if(senderEntity.getBankName().equalsIgnoreCase(receiverEntity.getBankName())) {
					TransactionEntity sendersResponse = transactionRepositoy.save(tEntity); 
					TransactionEntity receiverResponse = transactionRepositoy.save(tEntity);
					if(sendersResponse != null && receiverResponse != null) {
						boolean minusValue = minusUserBalance(request.getSendingId(), request.getTotalAmount());
						boolean plusValue = additionInUserBalance(request.getReceivingId(), request.getTotalAmount());
						if(minusValue && plusValue) {
							return 1;
						}
					}
				} 
				else {

				}
			}
		} else {
			List<Error> errorList = new ArrayList<>();
			Error error = new Error(400, "User not found", "User not found in database");
			errorList.add(error);
			throw new ValidationErrors(errorList);
		}
		return 0;
	}
	
	/**
	 * This method will receive transaction.
	 */
	@Override
	@Transactional
	public Integer receieveTransaction(TransactionRequest request) {
		TransactionEntity tEntity = mapper.convertRequestToTransactionEntity(request);
		TransactionEntity response = transactionRepositoy.save(tEntity);
		if(response != null) {
			additionInUserBalance(tEntity.getReceivingUser(), request.getTotalAmount());
			return 1;
		}
		
		return 0;
	}
	
	
	/**
	 * This method will be called when user pays amount.
	 * @param userId
	 * @param amount
	 * @return
	 */
	private boolean minusUserBalance(String userId, Integer amount) {
		UserEntity entity = userRepository.findByUserId(userId);
		entity.setBalance(entity.getBalance() - amount);
		UserEntity response = userRepository.save(entity);
		if(response != null) {
			return true;
		}
		return false;
		
	}
	
	/**
	 * This method will be called when user receieve amount.
	 * @param userId
	 * @param amount
	 * @return
	 */
	private boolean additionInUserBalance(String userId, Integer amount) {
		UserEntity entity = userRepository.findByUserId(userId);
		entity.setBalance(entity.getBalance() + amount);
		UserEntity response = userRepository.save(entity);
		if(response != null) {
			return true;
		}
		return false;
		
	}
}
